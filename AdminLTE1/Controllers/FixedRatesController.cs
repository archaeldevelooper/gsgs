﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AdminLTE1.Models;

namespace AdminLTE1.Controllers
{
    public class FixedRatesController : Controller
    {
        private GSMYSQL db = new GSMYSQL();

        // GET: FixedRates
        public ActionResult Index()
        {
            return View(db.FixedRates.ToList());
        }

        // GET: FixedRates/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FixedRate fixedRate = db.FixedRates.Find(id);
            if (fixedRate == null)
            {
                return HttpNotFound();
            }
            return View(fixedRate);
        }

        // GET: FixedRates/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: FixedRates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "FixedRateID,RateType,Rate,Other1,Other2,Other3")] FixedRate fixedRate)
        {
            if (ModelState.IsValid)
            {
                db.FixedRates.Add(fixedRate);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(fixedRate);
        }

        // GET: FixedRates/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FixedRate fixedRate = db.FixedRates.Find(id);
            if (fixedRate == null)
            {
                return HttpNotFound();
            }
            return View(fixedRate);
        }

        // POST: FixedRates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "FixedRateID,RateType,Rate,Other1,Other2,Other3")] FixedRate fixedRate)
        {
            if (ModelState.IsValid)
            {
                db.Entry(fixedRate).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(fixedRate);
        }

        // GET: FixedRates/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FixedRate fixedRate = db.FixedRates.Find(id);
            if (fixedRate == null)
            {
                return HttpNotFound();
            }
            return View(fixedRate);
        }

        // POST: FixedRates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            FixedRate fixedRate = db.FixedRates.Find(id);
            db.FixedRates.Remove(fixedRate);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
