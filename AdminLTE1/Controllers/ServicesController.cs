﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AdminLTE1.Models;
using System.IO;
using PagedList;

namespace AdminLTE1.Controllers
{
    public class ServicesController : Controller
    {
        private GSMYSQL db = new GSMYSQL();

        // GET: Services
        public ActionResult Index(int? pageNumber)
        {

            return View(db.Services.ToList());

        }

        // GET: Services/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Service service = db.Services.Find(id);
            if (service == null)
            {
                return HttpNotFound();
            }
            return View(service);
        }

        // GET: Services/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Services/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "ItemID,ItemName,CapacityType,Capacity,Photo,Specification,OtherCharges,Other1,Other2,Other3,OverrideRateID,FixedRateID")] Service service)
        public ActionResult Create (Service service , HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                //db.Services.Add(service);
                //db.SaveChanges();
                //return RedirectToAction("Index");

                string path = Path.Combine(Server.MapPath("/zxcimg42asfd"), Path.GetFileName(file.FileName));
                file.SaveAs(path);
                db.Services.Add(new Service
                {
                    ItemID = service.ItemID,
                    ItemName = service.ItemName,
                    Capacity = service.Capacity,
                    CapacityType = service.CapacityType,
                    Photo = "/zxcimg42asfd/" + file.FileName,
                    Specification = service.Specification,
                    OtherCharges = service.OtherCharges,
                    OverrideRateID = service.OverrideRateID,
                    FixedRateID = service.FixedRateID,
                });
                db.SaveChanges();
                return RedirectToAction("Index");

            }

            return View(service);
        }

        // GET: Services/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Service service = db.Services.Find(id);
            if (service == null)
            {
                return HttpNotFound();
            }
            return View(service);
        }

        // POST: Services/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Service service,HttpPostedFileBase file, int id)
        {
            if (ModelState.IsValid)
            {
              Service s = db.Services.Find(id);
                string path = Path.Combine(Server.MapPath("~/zxcimg42asfd"), Path.GetFileName(file.FileName));
                file.SaveAs(path);

                s.ItemID = service.ItemID;
                s.ItemName = service.ItemName;
                s.Capacity = service.Capacity;
                s.CapacityType = service.CapacityType;
                s.Photo = "~/zxcimg42asfd/" + file.FileName;
                s.Specification = service.Specification;
                s.OtherCharges = service.OtherCharges;
                s.OverrideRateID = service.OverrideRateID;
                s.FixedRateID = service.FixedRateID;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(service);
        }

        // GET: Services/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Service service = db.Services.Find(id);
            if (service == null)
            {
                return HttpNotFound();
            }
            return View(service);
        }

        // POST: Services/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Service service = db.Services.Find(id);
            db.Services.Remove(service);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
