namespace AdminLTE1.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gsDatabase.Customer")]
    public partial class Customer
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CustomerID { get; set; }

        [Required]
        [StringLength(45)]
        public string CustomerFname { get; set; }

        [Required]
        [StringLength(45)]
        public string CustomerMname { get; set; }

        [Required]
        [StringLength(45)]
        public string CustomerLname { get; set; }

        public int Contact { get; set; }

        [Required]
        [StringLength(150)]
        public string Address { get; set; }

        [StringLength(45)]
        public string Other1 { get; set; }

        [StringLength(45)]
        public string Other2 { get; set; }

        [StringLength(45)]
        public string Other3 { get; set; }
    }
}
