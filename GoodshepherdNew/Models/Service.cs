namespace GoodshepherdNew.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gsDatabase.Service")]
    public partial class Service
    {
        [Key]
        public int ItemID { get; set; }

        [Required]
        [StringLength(45)]
        public string ItemName { get; set; }

        [Required]
        [StringLength(45)]
        public string CapacityType { get; set; }
        [Required]
        public int Capacity { get; set; }

        [Required]
        [StringLength(45)]
        public string Specification { get; set; }

        [StringLength(45)]
        public string OtherCharges { get; set; }

   
        [StringLength(45)]
        public string Other1 { get; set; }

      
        [StringLength(45)]
        public string Other2 { get; set; }

  
        [StringLength(45)]
        public string Other3 { get; set; }

        public int FixedRate_FixedRateID { get; set; }

        public int OverrideRate_OverrideRateID { get; set; }

        public int Photos_PhotoID { get; set; }
    }
}
